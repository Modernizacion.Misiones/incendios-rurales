### **Ponderación del Riesgo y Predicción del Comportamiento de Incendios Rurales** ###

Open this link for access the repository in english language: **https://gitlab.com/Modernizacion.Misiones/rural-fires/**

El **Índice de Riesgo de Incendios Rurales (IRIR)** es una medida  de la probabilidad de ignición de un incendio. El cálculo del IRIR considera diversos factores, entre ellos la _disponibilidad e inflamabilidad del combustible_, los datos meteorológicos tomados diariamente a las 12:00 horas, la tasa de propagación del fuego y otros aspectos relevantes para determinar el **riesgo de inicio  de incendios en cada municipio**.

La disponibilidad de combustible y la inflamabilidad  se determinan a partir del análisis de imágenes satelitales y técnicas avanzadas de procesamiento de datos, para analizar la distribución de cobertura  de diferentes tipos de vegetación y su impacto en el riesgo de incendio local. En las imágenes satelitales se identifican y clasifican diferentes categorías de cobertura del suelo en la provincia de Misiones, incluyendo plantaciones y cultivos agrícolas, bosques nativos,forestaciones, pastizales, matorrales, etc.

La combinación de esta información con datos meteorológicos y la inflamabilidad de los combustibles proporciona una ponderación más precisa de la probabilidad de ignición y propagación de incendios en los municipios.

La propuesta del **Índice de Riesgo de Incendios Rurales** tendrá un impacto significativo en la comunidad y en las dependencias gubernamentales responsables de la gestión del fuego. El índice proporcionará una evaluación más precisa del riesgo de inicio de incendios, lo que permitirá una mejor planificación y preparación para la prevención y supresión del fuego. Esto podrá llevar a una reducción en el número y la gravedad de los incendios rurales, lo que a su vez protegerá la vida, la propiedad y el medio ambiente.

Para las oficinas gubernamentales, el índice proporcionará información detallada y específica sobre el riesgo de incendios en áreas rurales, lo que permitirá una toma de decisiones estratégicas en cuanto a la asignación de recursos para la prevención y respuesta a incendios. Además, al integrar datos en tiempo real y utilizar algoritmos computacionales avanzados, el índice podrá mejorar la capacidad del gobierno para anticipar y responder a los incendios de manera más eficiente.

En resumen, El IRIR es una herramienta que combina múltiples indicadores para proporcionar una evaluación integral del riesgo de incendio en áreas rurales, permitiendo a los administradores y autoridades de emergencias tomar decisiones mejor informadas sobre prevención y control de incendios.

![Descripción de la imagen](/imagenes/clasificacion-cultivos.png)

** Informes de los experimentos de campo **

https://gitlab.com/Modernizacion.Misiones/incendios-rurales/-/wikis/informe-experimento-01

https://gitlab.com/Modernizacion.Misiones/incendios-rurales/-/wikis/experimental-report-01/

https://gitlab.com/Modernizacion.Misiones/incendios-rurales/-/wikis/flammability
