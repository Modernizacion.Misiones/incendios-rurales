#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Lenguaje: Python 3
Propósito: Calcular el IRIR diario de los municipios
Archivo de entrada: IIM.csv (orden, nombre, latitud, longitud, índice de inflamabilidad)
Archivo de salida: IRIR.csv (codigo,nombre,latitud,longitud,IRIR,ultima_lluvia,velocidad_viento)
@author: Carlos Brys
"""

import os
import pandas as pd
import csv
import requests
import math
from datetime import datetime, timedelta

os.system('clear')

error = ""

API_KEY = "SU-API-KEY" 
# Por favor, obtenga su propia clave de API para producción
# Regístrese en https://www.visualcrossing.com/

# Leer el archivo CSV
# IIM - Indice de Inflamabilidad por municipios. 
# Esta tabla se genera combinando los centroides con desde la hoja de calculos de inflamabilidad del Drive
dataframe = pd.read_csv("../datos/IIM.csv") 
# codigo,nombre,latitud,longitud,indice_inflamabilidad

def obtener_datos_visual_crossing(latitud, longitud, fecha_inicio, fecha_fin):
    respuesta = requests.get(f"https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/{latitud},{longitud}/{fecha_inicio}/{fecha_fin}?key={API_KEY}&elements=datetime,temp,precip,humidity,windspeed,pressure&maxDistance=48&include=days&unitGroup=metric")
    if respuesta.status_code == 200:
        return respuesta.json()
    else:
        print(f"Error al obtener los datos: {respuesta.status_code}")
        return None

print("Procesando los datos de entrada de los municipios...")

# Crear un escritor CSV para el archivo de salida
# Abrir el archivo CSV en modo escritura
with open('../datos/IRIR.csv', 'w', newline='') as csvfile:
    escritor = csv.writer(csvfile)
    # Escribir la fila de encabezado
    escritor.writerow(['codigo','nombre','latitud','longitud','IRIR','ultima_lluvia','velocidad_viento','FHS'])
    for indice_fila in range(dataframe.shape[0]):
        # Obtener latitud y longitud
        codigo = dataframe.loc[indice_fila, "codigo"]
        nombre = dataframe.loc[indice_fila, "nombre"]
        latitud = dataframe.loc[indice_fila, "latitud"]
        longitud = dataframe.loc[indice_fila, "longitud"]
        # Índice de inflamabilidad
        indice_inflamabilidad = dataframe.loc[indice_fila, "indice_inflamabilidad"]
        # Obtener la fecha actual
        hoy = datetime.today()
        # Restar 8 días a la fecha actual
        fecha_inicio = hoy - timedelta(days=8)
        # Formatear la fecha a YYYY-MM-DD
        fecha_inicio = fecha_inicio.strftime("%Y-%m-%d")
        fecha_fin = hoy.strftime("%Y-%m-%d")
        # Obtener datos de Visual Crossing
        datos_visual_crossing = obtener_datos_visual_crossing(latitud, longitud, fecha_inicio, fecha_fin)
        # Mostrar datos de los últimos 7 días
        if datos_visual_crossing:
            # print("Comienzo ", "*" * 20)
            dias_lluvia = 10
            dias = 0
            for dia in datos_visual_crossing["days"]:
                dias = dias + 1
                precipitación = dia['precip']
                if precipitación > 5:  # debe llover más de 5mm para saturar la hojarasca
                    últimos_días_lluvia = (hoy - pd.to_datetime(dia['datetime'])).days
                #else:
                    # print("No llovió ese día")
                #print("-" * 20)
            velocidad_viento = dia['windspeed']
            # Calcular el Factor de Humedad del Suelo
            ulr = últimos_días_lluvia
            x = hoy.timetuple().tm_yday
            ds = 2 * math.sin(2 * (math.pi) * ((x-93)/366)) + 4
            FVV = velocidad_viento * 0.10  
            IIM = indice_inflamabilidad
            FHS = math.sqrt(ulr/ds)
            FVV = velocidad_viento * 0.10 # FVV es el 10% de la velocidad
            IRIR = int(IIM * FHS * FVV)
            # Escribir cada línea a medida que se completa
            print(codigo, nombre, IIM, FHS, FVV, IRIR)
            escritor.writerow([codigo, nombre, latitud, longitud, IRIR, ulr, velocidad_viento,FHS])
        else:
            print(f"No se encontraron datos para la ubicación: {latitud}, {longitud}")
print("Proceso finalizado.")
